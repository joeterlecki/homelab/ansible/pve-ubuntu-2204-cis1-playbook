# PVE Ubuntu 2204 CIS1 Playbook

This repo is a work in progress and includes a packer clone resource to build for proxmox

## Requirements

You must set the Proxmox Environment variables for token user and token secret

```hcl
variable "token" {
  type    = string
  default = env("PM_API_TOKEN_SECRET")
}

variable "username" {
  type    = string
  default = env("PM_API_TOKEN_ID")
```
