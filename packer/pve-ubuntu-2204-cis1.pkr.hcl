packer {
  required_plugins {
    proxmox = {
      version = ">= 1.1.0"
      source  = "github.com/hashicorp/proxmox"
    }
  }
}

source "proxmox-clone" "pve-ubuntu-2204" {
  clone_vm                 = var.clone_vm
  cloud_init               = var.cloud_init
  cloud_init_storage_pool  = var.cloud_init_storage_pool
  cores                    = var.cores
  cpu_type                 = var.cpu_type
  full_clone               = var.full_clone
  insecure_skip_tls_verify = var.insecure_skip_tls_verify
  memory                   = var.memory
  network_adapters {
    bridge   = var.network_bridge
    model    = var.network_model
    firewall = var.network_firewall
  }
  node                 = var.node
  onboot               = var.onboot
  os                   = var.os
  pool                 = var.pool
  proxmox_url          = var.proxmox_url
  qemu_agent           = var.qemu_agent
  scsi_controller      = var.scsi_controller
  ssh_username         = var.ssh_username
  ssh_password         = var.ssh_password
  ssh_private_key_file = var.ssh_private_key_file
  sockets              = var.sockets
  task_timeout         = var.task_timeout
  token                = "${var.token}"
  username             = "${var.username}"
  vm_id                = var.vm_id
  vm_name              = "${var.vm_name}-${formatdate("YYYYMM", timestamp())}"
}

build {
  sources = ["source.proxmox-clone.pve-ubuntu-2204"]
  provisioner "ansible" {
    playbook_file           = "${var.playbook}"
    user                    = "${var.ssh_username}"
    use_proxy = false
    extra_arguments = [
      "--become",
      "-u ${var.ssh_username}"
    ]
  }
}