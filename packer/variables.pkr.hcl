variable "clone_vm" {
  type    = string
  default = "ubuntu-jammy-2204-base"
}

variable "cloud_init" {
  type    = bool
  default = true
}

variable "cloud_init_storage_pool" {
  type    = string
  default = "synology"
}

variable "communicator" {
  type    = string
  default = "ssh"
}

variable "cores" {
  type    = number
  default = 1
}

variable "cpu_type" {
  type    = string
  default = "host"
}

variable "full_clone" {
  type    = bool
  default = true
}

variable "insecure_skip_tls_verify" {
  type    = bool
  default = true
}

variable "memory" {
  type    = number
  default = 2048
}

variable "network_bridge" {
  type    = string
  default = "vmbr0"
}

variable "network_model" {
  type    = string
  default = "virtio"
}

variable "network_firewall" {
  type    = bool
  default = false
}

variable "node" {
  type    = string
  default = "proxmox1"
}

variable "onboot" {
  type    = bool
  default = true
}

variable "os" {
  type    = string
  default = "l26"

}

variable "playbook" {
  type = string
  default = "../playbook.yml"
}

variable "pool" {
  type    = string
  default = "templates"
}

variable "proxmox_url" {
  type    = string
  default = "https://192.168.2.51:8006/api2/json"
}

variable "qemu_agent" {
  type    = bool
  default = true
}

variable "scsi_controller" {
  type    = string
  default = "virtio-scsi-pci"
}

variable "ssh_username" {
  type    = string
  default = "ubuntu"
}

variable "ssh_password" {
  type    = string
  default = "ubuntu"
}

variable "ssh_private_key_file" {
  type    = string
  default = "~/.ssh/proxmox"
}

variable "sockets" {
  type    = number
  default = 1
}

variable "task_timeout" {
  type    = string
  default = "5m"
}

variable "token" {
  type    = string
  default = env("PM_API_TOKEN_SECRET")
}

variable "username" {
  type    = string
  default = env("PM_API_TOKEN_ID")
}

variable "vm_id" {
  type    = number
  default = 9101
}

variable "vm_name" {
  type    = string
  default = "ubuntu-jammy-2204-cis1"
}